package eu.cz.cvut.fit.utils;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {

    public static Date getDateWithDaysOffset(int offset) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, offset);
        return c.getTime();
    }
}
