package eu.cz.cvut.fit.response;

public class NotFoundResource extends RuntimeException {
    public NotFoundResource(int id, String entity){
        super(entity + " with id " + id + " has not been found.");
    }
}
