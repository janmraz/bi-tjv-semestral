package eu.cz.cvut.fit.response;

import eu.cz.cvut.fit.model.Captain;
import eu.cz.cvut.fit.model.Race;

public class WinnerAlreadySet extends RuntimeException {
    public WinnerAlreadySet(Captain captain, Race race){
        super("Captain with id " + captain.getId() + " cannot be set as winner of the Race with id " + race.getId());
    }
}
