package eu.cz.cvut.fit.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Captain {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int credit;
    private String username;
    private String email;
    private String info;

    @ManyToOne(fetch = FetchType.EAGER)
    private Alliance alliance;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Race> races;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "winner")
    @JsonIgnore
    private Set<Race> wonRaces;

    public Captain() {
    }

    public Captain(int credit, String username, String email, String info) {
        this.credit = credit;
        this.username = username;
        this.email = email;
        this.info = info;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Alliance getAlliance() {
        return alliance;
    }

    public void setAlliance(Alliance alliance) {
        this.alliance = alliance;
    }

    public void addRace(Race race){
        races.add(race);
    }

    public void removeRace(Race race){
        races.removeIf(race1 -> race1.getId() == race.getId());
    }

    public Set<Race> getRaces() {
        return races;
    }

    public void setRaces(Set<Race> races) {
        this.races = races;
    }

    public Set<Race> getWonRaces() {
        return wonRaces;
    }

    public void setWonRaces(Set<Race> wonRaces) {
        this.wonRaces = wonRaces;
    }
}