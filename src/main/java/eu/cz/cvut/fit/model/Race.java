package eu.cz.cvut.fit.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Race {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int prize;
    private String name;
    private Date startDate;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "races")
    private Set<Captain> captains;

    @ManyToOne(fetch = FetchType.EAGER)
    private Captain winner;


    public Race() {
    }

    public Race(int prize, String name, Date startDate) {
        this.prize = prize;
        this.name = name;
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void addCaptain(Captain captain){
        captains.add(captain);
    }

    public void removeCaptain(Captain captain){
        captains.removeIf(cap -> cap.getId() == captain.getId());
    }

    public Set<Captain> getCaptains() {
        return captains;
    }

    public void setCaptains(Set<Captain> captains) {
        this.captains = captains;
    }

    public Captain getWinner() {
        return winner;
    }

    public boolean setWinner(Captain captain){ ;
        for (Iterator<Captain> it = captains.iterator(); it.hasNext(); ) {
            Captain f = it.next();
            if (f.getId() == captain.getId()){
                winner = captain;
                return true;
            }
        }
        return false;
    }

    public void resetWinner(){ ;
        winner = null;
    }
}
