package eu.cz.cvut.fit.repository;

import eu.cz.cvut.fit.model.Race;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RaceRepository extends JpaRepository<Race, Integer> {
}
