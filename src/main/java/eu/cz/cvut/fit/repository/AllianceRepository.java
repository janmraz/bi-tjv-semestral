package eu.cz.cvut.fit.repository;

import eu.cz.cvut.fit.model.Alliance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AllianceRepository extends JpaRepository<Alliance, Integer> {
}
