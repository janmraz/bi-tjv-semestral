package eu.cz.cvut.fit.repository;

import eu.cz.cvut.fit.model.Captain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaptainRepository extends JpaRepository<Captain, Integer> {
}
