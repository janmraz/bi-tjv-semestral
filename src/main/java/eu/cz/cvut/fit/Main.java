package eu.cz.cvut.fit;

import eu.cz.cvut.fit.model.Alliance;
import eu.cz.cvut.fit.model.Captain;
import eu.cz.cvut.fit.model.Race;
import eu.cz.cvut.fit.repository.AllianceRepository;
import eu.cz.cvut.fit.repository.CaptainRepository;
import eu.cz.cvut.fit.repository.RaceRepository;
import eu.cz.cvut.fit.utils.DateHelper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    CommandLineRunner initDatabase(CaptainRepository captainRepository, RaceRepository raceRepository, AllianceRepository allianceRepository){
        return args -> {
            captainRepository.save(new Captain(2000,"karel","karel@nope.com","doing nothing"));
            captainRepository.save(new Captain(1500,"Vasek","vasek@maaan.com","doing nothing exclusively"));
            raceRepository.save(new Race(3000,"quick race", DateHelper.getDateWithDaysOffset(-3)));
            raceRepository.save(new Race(5000,"dead end race", DateHelper.getDateWithDaysOffset(12)));
            allianceRepository.save(new Alliance("Alliance of regular dudes", 4000));
            allianceRepository.save(new Alliance("Alliance of mediocrity",300));
        };
    }

}
