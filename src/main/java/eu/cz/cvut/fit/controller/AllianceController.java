package eu.cz.cvut.fit.controller;

import eu.cz.cvut.fit.model.Alliance;
import eu.cz.cvut.fit.model.Captain;
import eu.cz.cvut.fit.repository.AllianceRepository;
import eu.cz.cvut.fit.repository.CaptainRepository;
import eu.cz.cvut.fit.response.NotFoundResource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AllianceController {
    private final AllianceRepository repository;
    private final CaptainRepository captainRepository;

    AllianceController(AllianceRepository repository,CaptainRepository captainRepository) {
        this.repository = repository;
        this.captainRepository = captainRepository;
    }

    @GetMapping("/alliances")
    List<Alliance> getAllAlliances() {
        return repository.findAll();
    }

    @PostMapping("/alliances")
    Alliance createAlliance(@RequestBody Alliance alliance) {
        Alliance newAlliance = new Alliance();
        newAlliance.setName(alliance.getName());
        newAlliance.setCredit(alliance.getCredit());
        return repository.save(newAlliance);
    }

    @PostMapping("/alliances/{id}/add-captain/{captainId}")
    Alliance addCaptain(@PathVariable Integer id,@PathVariable Integer captainId) {
        Captain captain = captainRepository.findById(captainId).orElseThrow(() -> new NotFoundResource(id, Captain.class.toString()));
        Alliance alliance = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Alliance.class.toString()));

        captain.setAlliance(alliance);
        captainRepository.save(captain);

        alliance.addCaptain(captain);
        return repository.save(alliance);
    }

    @PostMapping("/alliances/{id}/remove-captain/{captainId}")
    Alliance removeCaptain(@PathVariable Integer id,@PathVariable Integer captainId) {
        Captain captain = captainRepository.findById(captainId).orElseThrow(() -> new NotFoundResource(id, Captain.class.toString()));
        Alliance alliance = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Alliance.class.toString()));

        Alliance previousAlliance = captain.getAlliance();
        if(previousAlliance != null){
            previousAlliance.removeCaptain(captain);
            repository.save(previousAlliance);
        }

        captain.setAlliance(null);
        captainRepository.save(captain);

        alliance.removeCaptain(captain);
        return repository.save(alliance);
    }

    @GetMapping("/alliances/{id}")
    Alliance getOneAlliance(@PathVariable Integer id) {
        return repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Alliance.class.toString()));
    }

    @PutMapping("/alliances/{id}")
    Alliance updateAlliance(@RequestBody Alliance newAlliance, @PathVariable Integer id) {
        Alliance alliance = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Alliance.class.toString()));
        alliance.setName(newAlliance.getName());
        alliance.setCredit(newAlliance.getCredit());
        return repository.save(alliance);
    }

    @DeleteMapping("/alliances/{id}")
    void deleteAlliance(@PathVariable Integer id) {
        Alliance alliance = repository.findById(id).orElseThrow(() -> null);
        if(alliance != null){
            alliance.getCaptains().forEach(captain -> {
                captain.setAlliance(null);
                captainRepository.save(captain);
            });
        }
        repository.deleteById(id);
    }
}
