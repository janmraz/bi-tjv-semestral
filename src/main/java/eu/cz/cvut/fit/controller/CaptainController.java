package eu.cz.cvut.fit.controller;

import eu.cz.cvut.fit.model.Alliance;
import eu.cz.cvut.fit.model.Captain;
import eu.cz.cvut.fit.repository.AllianceRepository;
import eu.cz.cvut.fit.repository.CaptainRepository;
import eu.cz.cvut.fit.repository.RaceRepository;
import eu.cz.cvut.fit.response.NotFoundResource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CaptainController {
    private final CaptainRepository repository;
    private final AllianceRepository allianceRepository;
    private final RaceRepository raceRepository;

    CaptainController(CaptainRepository repository, AllianceRepository allianceRepository,RaceRepository raceRepository) {
        this.repository = repository;
        this.raceRepository = raceRepository;
        this.allianceRepository = allianceRepository;
    }

    @GetMapping("/captains")
    List<Captain> getAllCaptains() {
        return repository.findAll();
    }

    @PostMapping("/captains")
    Captain createCaptain(@RequestBody Captain captain) {
        Captain newCaptain = new Captain();
        newCaptain.setInfo(captain.getInfo());
        newCaptain.setEmail(captain.getEmail());
        newCaptain.setCredit(captain.getCredit());
        newCaptain.setUsername(captain.getUsername());
        return repository.save(newCaptain);
    }

    @GetMapping("/captains/{id}")
    Captain getOneCaptain(@PathVariable Integer id) {
        return repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Captain.class.toString()));
    }

    @PutMapping("/captains/{id}")
    Captain updateCaptain(@RequestBody Captain newCaptain, @PathVariable Integer id) {
        Captain captain = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Captain.class.toString()));
        captain.setAlliance(newCaptain.getAlliance());
        captain.setUsername(newCaptain.getUsername());
        captain.setCredit(newCaptain.getCredit());
        captain.setEmail(newCaptain.getEmail());
        captain.setInfo(newCaptain.getInfo());
        return repository.save(captain);
    }

    @DeleteMapping("/captains/{id}")
    void deleteCaptain(@PathVariable Integer id) {
        Captain captain = repository.findById(id).orElseThrow(() -> null);
        if(captain != null){
            captain.getRaces().forEach(race -> {
                race.removeCaptain(captain);
                if(race.getWinner() != null && race.getWinner().getId() == id){
                    race.resetWinner();
                }
                raceRepository.save(race);
            });
            Alliance alliance = captain.getAlliance();
            if(alliance != null){
                alliance.removeCaptain(captain);
                allianceRepository.save(alliance);
            }
        }

        repository.deleteById(id);
    }
}
