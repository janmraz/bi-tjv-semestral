package eu.cz.cvut.fit.controller;

import eu.cz.cvut.fit.model.Captain;
import eu.cz.cvut.fit.model.Race;
import eu.cz.cvut.fit.repository.CaptainRepository;
import eu.cz.cvut.fit.repository.RaceRepository;
import eu.cz.cvut.fit.response.InvalidCaptain;
import eu.cz.cvut.fit.response.NotFoundResource;
import eu.cz.cvut.fit.response.WinnerAlreadySet;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RaceController {
    private final RaceRepository repository;
    private final CaptainRepository captainRepository;

    RaceController(RaceRepository repository, CaptainRepository captainRepository) {
        this.repository = repository;
        this.captainRepository = captainRepository;
    }

    @GetMapping("/races")
    List<Race> getAllRaces() {
        return repository.findAll();
    }

    @PostMapping("/races")
    Race createRace(@RequestBody Race race) {
        Race newRace = new Race();
        newRace.setName(race.getName());
        newRace.setPrize(race.getPrize());
        newRace.setStartDate(race.getStartDate());
        return repository.save(newRace);
    }

    @PostMapping("/races/{id}/add-captain/{captainId}")
    Race addCaptain(@PathVariable Integer id, @PathVariable Integer captainId) {
        Captain captain = captainRepository.findById(captainId).orElseThrow(() -> new NotFoundResource(id, Captain.class.toString()));
        Race race = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Race.class.toString()));

        captain.addRace(race);
        captainRepository.save(captain);

        race.addCaptain(captain);
        return repository.save(race);
    }

    @PostMapping("/races/{id}/remove-captain/{captainId}")
    Race removeCaptain(@PathVariable Integer id, @PathVariable Integer captainId) {
        Captain captain = captainRepository.findById(captainId).orElseThrow(() -> new NotFoundResource(id, Captain.class.toString()));
        Race race = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Race.class.toString()));

        captain.removeRace(race);
        captainRepository.save(captain);

        race.removeCaptain(captain);
        return repository.save(race);
    }

    @PostMapping("/races/{id}/winner/{captainId}")
    Race setWinner(@PathVariable Integer id, @PathVariable Integer captainId) {
        Captain captain = captainRepository.findById(captainId).orElseThrow(() -> new NotFoundResource(id, Captain.class.toString()));
        Race race = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Race.class.toString()));

        if(race.getWinner() != null){
            throw new WinnerAlreadySet(captain, race);
        }

        if(!race.setWinner(captain)){
            throw new InvalidCaptain(captain, race);
        }

        return repository.save(race);
    }

    @GetMapping("/races/{id}")
    Race getOneRace(@PathVariable Integer id) {
        return repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Race.class.toString()));
    }

    @PutMapping("/races/{id}")
    Race updateRace(@RequestBody Race newRace, @PathVariable Integer id) {
        Race race = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Race.class.toString()));
        race.setName(newRace.getName());
        race.setPrize(newRace.getPrize());
        race.setStartDate(newRace.getStartDate());
        return repository.save(race);
    }

    @DeleteMapping("/races/{id}")
    void deleteRace(@PathVariable Integer id) {
        Race race = repository.findById(id).orElseThrow(() -> new NotFoundResource(id, Race.class.toString()));
        // or throw error that user cannot delete race that has any captains
        if(race != null){
            race.getCaptains().forEach(captain -> {
                captain.removeRace(race);
                captainRepository.save(captain);
            });
        }
        repository.deleteById(id);
    }
}
